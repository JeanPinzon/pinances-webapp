import {
  AUTHENTICATING_USER,
  SUCCESS_TO_AUTHENTICATE_USER,
  FAILED_TO_AUTHENTICATE_USER,
  SUCCESS_TO_LOGOUT_USER,
} from '../ActionTypes'


let loggedUser = localStorage.getItem('loggedUser')

if (loggedUser) {
  loggedUser = JSON.parse(loggedUser)
}


const initialState = {
  loading: false,
  error: undefined,
  loggedUser: loggedUser,
}

const auth = (state = initialState, action) => {

  switch (action.type) {

    case SUCCESS_TO_AUTHENTICATE_USER:
      return {
        ...initialState,
        loggedUser: action.loggedUser
      }

    case FAILED_TO_AUTHENTICATE_USER:
      return {
        ...initialState,
        loggedUser: undefined,
        error: action.error,
        loading: false
      }

    case AUTHENTICATING_USER:
      return {
        ...initialState,
        loading: true
      }

    case SUCCESS_TO_LOGOUT_USER:
        return {
          ...initialState,
          loggedUser: null,
          error: null,
          loading: false
        }

    default:
      return state
  }
}

export default auth
