import {
  GETTING_EXPENSES,
  SUCCESS_TO_GET_EXPENSES,
  FAILED_TO_GET_EXPENSES
} from '../ActionTypes'


const initialState = {
  loading: false,
  error: undefined,
  expenses: [],
}

const expenses = (state = initialState, action) => {

  switch (action.type) {

    case SUCCESS_TO_GET_EXPENSES:
      return {
        ...initialState,
        expenses: action.expenses
      }

    case FAILED_TO_GET_EXPENSES:
      return {
        ...initialState,
        expenses: [],
        error: action.error,
        loading: false
      }

    case GETTING_EXPENSES:
      return {
        ...initialState,
        loading: true
      }

    default:
      return state
  }
}

export default expenses
