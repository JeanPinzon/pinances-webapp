import localConf from "./local"
import prodConf from "./prod"

const environment = {
  development: localConf,
  production: prodConf,
}[process.env.APP_ENV]

if (!environment) {
  throw new Error(`Environment config not valid: ${environment}`)
}

export default environment
