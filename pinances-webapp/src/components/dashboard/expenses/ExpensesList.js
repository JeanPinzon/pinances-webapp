import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import {
  CircularProgress, 
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core'

import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import { makeStyles } from '@material-ui/core/styles'

import { getExpenses } from "../../../actions/expenses"

const useStyles = makeStyles(theme => ({
  title: {
    marginBottom: theme.spacing(2),
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  }
}))

export default function ExpensesList(props) {
  const classes = useStyles()
  const { loading, expenses, error } = useSelector(state => state.expenses)
  const dispatch = useDispatch()

  useEffect(() => { getExpenses(dispatch) }, [])

  if (error) {
    return <h1>{ error }</h1>
  }

  return (
    <>
      <Typography className={classes.title} variant="h6">
        Despesas
      </Typography>
      <Paper>
      {
        loading ? 
        <CircularProgress /> :  
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Descrição</TableCell>
              <TableCell>Valor</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {expenses.map(expense => (
              <TableRow key={expense.id}>
                <TableCell>{expense.description}</TableCell>
                <TableCell>
                  {`R$ ${expense.value.toFixed(2).replace(".", ",")}`}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      }
      </Paper>
      <Fab color="primary" aria-label="add" className={classes.fab}>
        <AddIcon />
      </Fab>
    </>
  )
}