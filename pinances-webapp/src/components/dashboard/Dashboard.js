import React, { useState } from 'react'

import { Route, withRouter } from 'react-router-dom'

import { 
  Container, 
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  SwipeableDrawer,
  Typography,
} from "@material-ui/core"

import HomeIcon from '@material-ui/icons/Home'
import MoneyOffIcon from '@material-ui/icons/MoneyOff'
import MoneyIcon from '@material-ui/icons/AttachMoney'

import { makeStyles } from '@material-ui/core/styles'

import TopBar from '../top-bar/TopBar'
import ExpensesList from "./expenses/ExpensesList"

const DashboardContent = () => (
  <>
    <Typography variant="h4">
      Seja bem vindo ao Pinances!
    </Typography>
    <Typography variant="body1">
      Tenho que arranjar algo pra colocar aqui no dashboard. Por enquanto, use o menu =)
    </Typography>
  </>
)

const MENU_ITEMS = [{
  key: 'home',
  name: 'Início',
  route: '/dashboard',
  exact: true,
  component: DashboardContent,
  icon: <HomeIcon />,
}, {
  key: 'incomes',
  name: 'Receitas',
  route: '/dashboard/receitas',
  exact: false,
  component: ExpensesList,
  icon: <MoneyIcon />,
}, {
  key: 'expenses',
  name: 'Despesas',
  route: '/dashboard/despesas',
  exact: false,
  component: ExpensesList,
  icon: <MoneyOffIcon />,
}]

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(12),
    marginBottom: theme.spacing(4),
  },
  menuListItem: {
    paddingRight: theme.spacing(6)
  }
}))

const Dashboard = ({ history, location }) => {
  const classes = useStyles()
  const [isMenuOpened, setIsMenuOpened] = useState(false)

  const closeMenu = () => setIsMenuOpened(false)
  const openMenu = () => setIsMenuOpened(true)

  const redirect = route => history.push(route)

  return (
    <>
      <TopBar onMenuClick={openMenu} />
      <SwipeableDrawer
        open={isMenuOpened}
        onClose={openMenu}
        onOpen={closeMenu}
      >
        <div
          role="presentation"
          onClick={closeMenu}
          onKeyDown={closeMenu}
        >
          <List>
            {MENU_ITEMS.map((menuItem) => (
              <ListItem 
                className={classes.menuListItem}
                button 
                key={menuItem.key} 
                selected={location.pathname === menuItem.route}
                onClick={() => redirect(menuItem.route)}
              >
                <ListItemIcon>{menuItem.icon}</ListItemIcon>
                <ListItemText primary={menuItem.name} />
              </ListItem>
            ))}
          </List>
        </div>
      </SwipeableDrawer>
      <Container className={classes.container}>
        {
          MENU_ITEMS.map((menuItem) => 
            <Route 
              key={menuItem.key} 
              path={menuItem.route} 
              component={menuItem.component} 
              exact={menuItem.exact}/>
          )
        }
      </Container>
    </>
  )
}

export default withRouter(Dashboard)
