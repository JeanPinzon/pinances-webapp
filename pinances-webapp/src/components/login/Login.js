import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { Button, Typography } from "@material-ui/core"
import { Redirect } from 'react-router-dom'
import { GoogleLogin } from 'react-google-login'
import { loginWithGoogle } from "../../actions/auth"

const useStyles = makeStyles(theme => ({
  loginPage: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: theme.spacing(24),
  },
  loginTitle: {
    marginBottom: theme.spacing(4),
  },
}))

const LoginButton = props => (
  <Button onClick={props.onClick} variant="contained" size="large" color="primary">
    Entrar
  </Button>
)

const Login = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  
  const { loading, loggedUser, error } = useSelector(state => state.auth)
  const [googleError, setGoogleError] = useState(false)

  const onGoogleLoginSuccess = res => {
    setGoogleError(false)
    loginWithGoogle(dispatch, res)
  }
  
  const onGoogleLoginError = () => setGoogleError(true)

  if (loading) {
    return <h1>guentafirme</h1>
  }

  if (loggedUser) {
    return <Redirect to={{ pathname: '/dashboard' }} />
  }

  return (
    <div className={classes.loginPage}>
      <Typography variant="h6" className={classes.loginTitle}>
        Seja bem vindo ao Pinances!
      </Typography>
      <GoogleLogin
        clientId='519635575360-lhn80duauglmo19otquvtpqbsnqa2966.apps.googleusercontent.com'
        buttonText='Login'
        onSuccess={onGoogleLoginSuccess}
        onFailure={onGoogleLoginError}
        render={props => <LoginButton onClick={props.onClick} />}
      />
      {error && <h1>{error}</h1>}
      {googleError && <h1>Algum pau com o google</h1>}
    </div>
  )
}

export default Login