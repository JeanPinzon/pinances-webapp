import React from 'react'
import { useSelector } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'


const PrivateRoute = ({ component: Component, ...rest }) => {
  const loggedUser = useSelector(state => state.auth.loggedUser)

  return (
    <Route {...rest} render={(props) => (
      loggedUser ?
        <Component {...props} /> : 
        <Redirect to={{
          pathname: '/login',
          state: { from: props.location }
        }} />
    )} />
  )
}

export default PrivateRoute
