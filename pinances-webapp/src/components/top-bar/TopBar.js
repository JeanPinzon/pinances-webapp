import React from 'react'

import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'

import { makeStyles } from '@material-ui/core/styles'

import MenuIcon from '@material-ui/icons/Menu'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'

import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
} from '@material-ui/core'

import { logout } from "../../actions/auth"


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  loggedUserName: {
    marginRight: theme.spacing(1),
  },
}))


const TopBar = props => {
  const classes = useStyles()
  const loggedUserName = useSelector(state => state.auth.loggedUser.name)
  const dispatch = useDispatch()

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton className={classes.menuButton} color="inherit" onClick={props.onMenuClick}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Pinances
          </Typography>
          <Typography variant="body1" className={classes.loggedUserName}>
              {loggedUserName}
          </Typography>
          <IconButton color="inherit" onClick={() => logout(dispatch)}>
            <ExitToAppIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  )
}

TopBar.propTypes = {
  onMenuClick: PropTypes.func.isRequired,
}

export default TopBar