import {
  AUTHENTICATING_USER,
  FAILED_TO_AUTHENTICATE_USER,
  SUCCESS_TO_AUTHENTICATE_USER,
  SUCCESS_TO_LOGOUT_USER,
} from '../ActionTypes'

import store from '../store'
import conf from '../conf'


const _authenticateUser = async (googleResponse) => {
  const loggedUser = {
    email: googleResponse.profileObj.email,
    name: googleResponse.profileObj.name,
    googleIdToken: googleResponse.tokenId,
  }

  const url = `${conf.apiUrl}/api/v1/auth/${googleResponse.tokenId}`

  const response = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(loggedUser)
  })

  const responseJson = await response.json()

  return {
    email: googleResponse.profileObj.email,
    name: googleResponse.profileObj.name,
    token: responseJson.token
  }
}

const _saveUserIntoLocalStorage = (loggedUser) => {
  localStorage.setItem('loggedUser', JSON.stringify(loggedUser))
}

export const logout = async (dispatch) => {
  localStorage.removeItem('loggedUser')

  const url = `${conf.apiUrl}/api/v1/auth/${store.getState().auth.loggedUser.token}`

  await fetch(url, { method: 'DELETE' })

  dispatch({
    type: SUCCESS_TO_LOGOUT_USER,
  })
}

export const loginWithGoogle = async (dispatch, googleResponse) => {
  try {
    dispatch({ type: AUTHENTICATING_USER })

    const loggedUser = await _authenticateUser(googleResponse)

    _saveUserIntoLocalStorage(loggedUser)

    dispatch({
      type: SUCCESS_TO_AUTHENTICATE_USER,
      loggedUser,
    })
  } catch (e) {
    dispatch({ type: FAILED_TO_AUTHENTICATE_USER, error: 'fodeu motherfucker' })
  }
}
