import {
  GETTING_EXPENSES,
  FAILED_TO_GET_EXPENSES,
  SUCCESS_TO_GET_EXPENSES,
  FAILED_TO_AUTHENTICATE_USER,
} from '../ActionTypes'

import store from '../store'
import conf from '../conf'


const _requestExpenses = async () => {
  const url = `${conf.apiUrl}/api/v1/expenses/`

  const response = await fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': store.getState().auth.loggedUser.token,
    }
  })

  if (!response.ok) {
    throw { response, httpError: true }
  }

  const responseJson = await response.json()

  return responseJson.map(i => JSON.parse(i))
}

export const getExpenses = async (dispatch) => {
  try {
    dispatch({ type: GETTING_EXPENSES })

    const expenses = await _requestExpenses()

    dispatch({
      type: SUCCESS_TO_GET_EXPENSES,
      expenses,
    })
  } catch (e) {
    if (e.httpError && e.response.status === 403) {
      dispatch({ type: FAILED_TO_AUTHENTICATE_USER, error: 'te conheço?' })
    } else {
      dispatch({ type: FAILED_TO_GET_EXPENSES, error: 'fodeu motherfucker' })
    }
  }
}
