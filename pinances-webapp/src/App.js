import React from 'react'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles'
import purple from '@material-ui/core/colors/purple'
import green from '@material-ui/core/colors/green'

import Login from './components/login/Login'
import Dashboard from './components/dashboard/Dashboard'
import PrivateRoute from './components/common/PrivateRoute'

import store from './store'

const theme = createMuiTheme({
  palette: {
    primary: purple,
    secondary: green,
  },
})

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path='/' component={Login} exact/>
            <Route path='/login' component={Login} />
            <PrivateRoute path='/dashboard' component={Dashboard} />
          </Switch>
        </Router>
      </Provider>
    </ThemeProvider>
  )
}

export default App
